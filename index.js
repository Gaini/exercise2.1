const http = require('http');
const fs = require('file-system');
const qs = require('querystring');

const transl = require('./getTranslate');



const server = http.createServer( function(req, res) {

    if (req.method == 'POST') {

        function MakeAnswer(data) {
            //console.log(data);
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write('<p><b>');
            res.write(data);
            res.write('</b></p>');
            res.end();
        }

        var data = '';
        req.on('data', function (chunk) {
            data += chunk;
        });
        req.on('end', function () {
            var post = qs.parse(data);
            transl.send(post['textarea-ru']).then(data => MakeAnswer(data));
        });
    }
    else
    {
        var html = fs.readFileSync('index.html');
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end(html);
    }

});

const port = 3000;
const host = '127.0.0.1';
server.listen(port, host);
console.log('Listening at http://' + host + ':' + port);