const https = require('https');
const url = 'https://translate.yandex.net/api/v1.5/tr.json/translate';

function send(word) {
    return new Promise(function (resolve, reject) {
        const querystring = require('querystring');
        let senddata = querystring.stringify({
            'key': 'trnsl.1.1.20160723T183155Z.f2a3339517e26a3c.d86d2dc91f2e374351379bb3fe371985273278df',
            'text': word.toString(),
            'lang': 'ru-en',
        });

        const request = https.request(url + '?' + senddata);
        request.on('response', function (response) {
            let data = '';
            response.on('data', function (chunk) {
                data += chunk;
            });
            response.on('end', function () {
                result = JSON.parse(data).text.toString();
                resolve(result);
            });
        });
        request.end();
    })
}

module.exports.send = send;